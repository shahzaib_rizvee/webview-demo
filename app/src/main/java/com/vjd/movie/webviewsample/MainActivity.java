package com.vjd.movie.webviewsample;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    public static final String URL = "https://azm.to/";
    private static final String TAG = "MainActivity";
    WebView mWebView;
    boolean done = false;
    String sessionCookie;
    private OkHttpClient client = null;
    private int cacheSize = 20 * 1024 * 1024; // 20MB

    public static void clearCookies(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            Log.d(TAG, "Using clearCookies code for API >=" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            Log.d(TAG, "Using clearCookies code for API <" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mWebView = findViewById(R.id.mWebView);
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
    mWebView.setWebViewClient(getWebClient());

        android.webkit.CookieSyncManager.createInstance(this);
// unrelated, just make sure cookies are generally allowed
        android.webkit.CookieManager.getInstance().setAcceptCookie(true);

// magic starts here
         coreCookieManager = new WebkitCookieManagerProxy(null, java.net.CookiePolicy.ACCEPT_ALL);
        java.net.CookieHandler.setDefault(coreCookieManager);

      //  clearCookies(MainActivity.this);


        mWebView.loadUrl(URL);

    }
    WebkitCookieManagerProxy coreCookieManager = null;
    public WebViewClient getWebClient() {
        WebViewClient client = new WebViewClient() {
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                Log.d(TAG, "should Intercept Request for URL: "+url);
                Request okHttpRequest = new Request.Builder().url(url).build();
//                okHttpRequest.headers()
                try {
                    Response response = getOkHTTPClient().newCall(okHttpRequest).execute();
                    List<String> headers = response.headers("Set-Cookie");
                    for (String setCookieHeader :headers) {
                        CookieManager.getInstance().setCookie(url, setCookieHeader);
                    }
//                    mWebView.loadDataWithBaseURL(
//                            response.request().url().toString(),
//                            response.body().string(),
//                            response.header("Content-Type")!=null ? response.header("Content-Type") : "text/html",
//
//		);
                    return new WebResourceResponse("","",response.body().byteStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        };
        return client;
    }

    public OkHttpClient getOkHTTPClient() {
        if (client == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();

//            builder.cookieJar(new CookieJar() {
//                @Override
//                public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
//
//                }
//
//                @Override
//                public List<Cookie> loadForRequest(HttpUrl url) {
//                    return !sessionCookie.isEmpty() ? Arrays.asList(Cookie.parse(url, sessionCookie)) : Collections.EMPTY_LIST;
//                }
//            });
            builder.cache(new Cache(getCacheDir(), cacheSize));
            client = builder.build();
        }
        return client;
    }
//
//    private void downloadMoviesHomePage(String url) {
//
//        Request request = new Request.Builder().url(url).build();
//        if (sessionCookie != null) {
//            client = new OkHttpClient.Builder().cookieJar(new CookieJar() {
//                @Override
//                public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
//
//                }
//
//                @Override
//                public List<Cookie> loadForRequest(HttpUrl url) {
//                    return !sessionCookie.isEmpty() ? Arrays.asList(Cookie.parse(url, sessionCookie)) : Collections.EMPTY_LIST;
//                }
//            }).cache(new Cache(getCacheDir(), cacheSize)).build();
//        }
//        client.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(@NonNull Call call, @NonNull IOException e) {
//                Log.d(TAG, "onFailure: " + e.getMessage());
//            }
//
//            @Override
//            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
//                if (response.body() == null)
//                    return;
//
//                String page = response.body().string();
//                Document doc = Jsoup.parse(page);
//
//                Elements moviesList = doc.select("body>div>a");
//
//                if (moviesList.size() > 0) {
//                    Log.d(TAG, "Response Recieved");
//                } else {
//                    Log.d(TAG, "Response Not Recieved");
//                }
//
//            }
//        });
//    }
//
//    class MyWebViewClient extends WebViewClient {
//
//        @Override
//        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
//            Log.d(TAG, "Redirected To " + url);
//            return false;
//        }
//
//        @Override
//        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
//            Log.d(TAG, "Request Headers" + request.getRequestHeaders());
//            if (request.getUrl().toString().equalsIgnoreCase("https://azm.to/ajax/captcha.php")) {
//                done = true;
//            }
//            Log.d(TAG, "Request URL" + request.getUrl());
//            return null;
//        }
//
//        @Override
//        public void onPageFinished(WebView view, String url) {
//
//            super.onPageFinished(view, url);
//
//            CookieSyncManager syncManager = CookieSyncManager.createInstance(mWebView.getContext());
//            CookieManager cookieManager = CookieManager.getInstance();
//
//            String cookie = cookieManager.getCookie(url);
//            sessionCookie = cookie;
//            syncManager.sync();
//            Log.d(TAG, "Cookies: " + cookie);
//
//            if (done) {
//                downloadMoviesHomePage(url);
//            }
//
//            Log.d(TAG, "PAGE FINISHED LOADING WITH URL : " + url);
//
//        }
//    }
}
